name = "rv_core"

version = "0.1.1"

description = """
    Core Locksmith configuration and tools for RV.
    """

tools = []

requires = []

build_requires = [
    "locksmith_dev",
]


@early()
def build_command():
    from rez_shared.build import build

    return build.create_build_command(this)


uuid = "locksmith_rv_core"
