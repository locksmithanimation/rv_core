from tank.platform.qt import QtGui, QtCore

class Publish_Dialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Publish_Dialog, self).__init__(parent)

        # size policy
        self.resize(250, 208)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QtCore.QSize(250, 208))
        self.setMaximumSize(QtCore.QSize(260, 208))

        # layouts and widgets
        self.verticalLayout_17 = QtGui.QVBoxLayout(self)
        self.verticalLayout_17.setSpacing(0)
        self.verticalLayout_17.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_17.setObjectName("verticalLayout_17")

        self.horizontalFrame = QtGui.QFrame(self)
        self.horizontalFrame.setObjectName("horizontalFrame")
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalFrame)
        self.horizontalLayout_3.setSpacing(10)
        self.horizontalLayout_3.setContentsMargins(10, 0, 10, -1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")

        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setSpacing(2)
        self.horizontalLayout_4.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")

        # sequence widgets
        self.label = QtGui.QLabel(self.horizontalFrame)
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.sequence_cmb = QtGui.QComboBox(self.horizontalFrame)
        self.sequence_cmb.setObjectName("sequence_cmb")
        self.horizontalLayout_4.addWidget(self.sequence_cmb)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSpacing(2)
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout.setObjectName("horizontalLayout")

        # action widgets
        self.label_2 = QtGui.QLabel(self.horizontalFrame)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.action_entity_cmb = QtGui.QComboBox(self.horizontalFrame)
        self.action_entity_cmb.setObjectName("action_entity_cmb")
        self.horizontalLayout.addWidget(self.action_entity_cmb)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(10)
        self.horizontalLayout_2.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.label_3 = QtGui.QLabel(self.horizontalFrame)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.comment_txt = QtGui.QPlainTextEdit(self.horizontalFrame)
        self.comment_txt.setObjectName("comment_txt")
        p = self.comment_txt.palette()
        p.setColor(QtGui.QPalette.Text, QtGui.QColor(200,200,200))
        self.comment_txt.setPalette(p)
        self.horizontalLayout_2.addWidget(self.comment_txt)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setSpacing(5)
        self.horizontalLayout_5.setContentsMargins(5, 5, 5, 5)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")

        spacerItem = QtGui.QSpacerItem(50, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem)

        # publish button widget
        b1 = QtGui.QPushButton("Publish", self.horizontalFrame)
        self.buttons = QtGui.QDialogButtonBox()
        self.buttons.addButton(b1, QtGui.QDialogButtonBox.AcceptRole)
        self.buttons.accepted.connect(self.exit_dialog)
        self.sequence = None
        self.action = None
        self.success = False

        self.horizontalLayout_5.addWidget(self.buttons)
        spacerItem1 = QtGui.QSpacerItem(50, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem1)

        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_17.addWidget(self.horizontalFrame)

        # set default values
        self.setWindowTitle("Publish Settings")
        self.label.setText("Sequence")
        self.label_2.setText("Action entity")
        self.label_3.setText("Comment")

    def pop_sequences(self, sequences):
        for key, val in sequences.iteritems():
            self.sequence_cmb.addItem(key)

    def pop_actions(self, entities_dict):
        for key, val in entities_dict.iteritems():
            self.action_entity_cmb.addItem(key)

    def get_comment(self):
        return self.comment_txt.toPlainText()

    def exit_dialog(self):
        self.sequence = self.sequence_cmb.currentText()
        self.action = self.action_entity_cmb.currentText()
        self.close()
        self.success = True

    def show(self):
        self.exec_()
