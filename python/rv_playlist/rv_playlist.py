import os
import sys
import platform

executable_dir = os.path.dirname(os.environ["RV_APP_RV"])
if (platform.system == "Darwin"):
    content_dir = os.path.split(os.path.split(executable_dir)[0])[0]
else:
    content_dir = os.path.split(executable_dir)[0]
bundle_cache_dir = os.path.join(content_dir, "src", "python", "sgtk", "bundle_cache")
core = os.path.join(bundle_cache_dir, "manual", "tk-core", "v1.0.43")
core = os.environ.get("RV_TK_CORE") or core
core = os.path.join(core, "python")
sys.path.insert(0, core)
sys.path.insert(0, "Z:\\shotgun\\configs\\studio\\install\\core\\python")

from tank.platform.qt import QtGui, QtCore
from .ui.rv_playlist_dialog import Ui_RvPlaylist
from publish_dialog import Publish_Dialog
from pymu import MuSymbol
import rv
import rv.commands as rvc

from .caches import ShotgunCaches
from wip_playblasts.playblast_publisher import PlayblastPublisher

def setup(parent):
    playlist_dock = QtGui.QDockWidget("", parent)
    playlist_dock.setFocusPolicy(QtCore.Qt.NoFocus)
    playlist_dock.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
    playlist_dock.setTitleBarWidget(QtGui.QWidget(parent))
    playlist_dock.setStyleSheet("QToolButton {\n    border: none;\n    border-radius: 0px;\n    min-width: 0px;\n    min-height: 0px;\n    margin: 0px;\n    background: transparent;\n}")
    playlist = RvPlaylist(parent)
    playlist_dock.setWidget(playlist)
    parent.addDockWidget(QtCore.Qt.RightDockWidgetArea, playlist_dock)
    playlist.dock = playlist_dock
    return playlist_dock


class RvPlaylist(QtGui.QWidget):

    def __init__(self, parent):
        super(RvPlaylist, self).__init__(parent)
        ShotgunCaches.init_caches()
        self.ui = Ui_RvPlaylist()
        self.ui.setupUi(self)
        self._dock = None
        self._dock_title = None
        self.publish_dialog = None
        self.ui.close_button.clicked.connect(self._close)
        self.ui.float_button.clicked.connect(self._toggle_floating)
        self.ui.add_btn.clicked.connect(self._on_add)
        self.ui.minus_btn.clicked.connect(self._on_minus)
        self.ui.clear_btn.clicked.connect(self._on_clear)
        self.ui.refresh_btn.clicked.connect(self._on_refresh)
        self.ui.save_btn.clicked.connect(self._on_save)
        self.ui.open_btn.clicked.connect(self._on_open)
        self.ui.publish_btn.clicked.connect(self._on_publish)

    @property
    def dock(self):
        return self._dock

    @dock.setter
    def dock(self, value):
        self._dock = value
        self._dock_title = value.titleBarWidget()
        self._dock.topLevelChanged.connect(self._on_float_changed)

    def _on_add(self):
        self.ui.cut_lst.add_item()

    def _on_minus(self):
        self.ui.cut_lst.remove_item()

    def _on_clear(self):
        self.ui.cut_lst.clear_list()

    def _on_refresh(self):
        self.ui.cut_lst.refresh()

    def _on_save(self):
        file_path = QtGui.QFileDialog.getSaveFileName(self, "Save Cut", "c:\\Temp", "Json File (*.json)")
        cut = self.ui.cut_lst.cut
        cut.to_json(file_path[0])

    def _on_open(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self, "Open Cut", "c:\\Temp", "Json File (*.json)")
        self.ui.cut_lst.load_from_file(file_name[0])

    def _close(self):
        self.dock.hide()

    def _on_publish(self):
        user = self.session_data()[1]
        cut = self.ui.cut_lst.cut
        if not cut:
            return
        publisher = PlayblastPublisher(cut=cut, take_filter=[], comment="test")
        if user:
            publisher.current_user = user
        actions, sequences = publisher.get_sequences_and_actions()
        pd = Publish_Dialog()
        pd.pop_actions(actions)
        pd.pop_sequences(sequences)
        pd.show()
        if pd.success:
            # set selected sequence and action back to playblast_publisher
            publisher.set_sequence(sequences[pd.sequence])
            publisher.set_action(actions[pd.action])
            publisher.set_comment(pd.get_comment())

            #self._on_clear()
            publisher.update_version()
            #self.ui.cut_lst.load_from_file(json_obj=output_dict)
            publisher.do_publish(inspect_version=True)

    def session_data(self):
        """
        :return:  list containing shotgun url, current user, token
        """
        rv.runtime.eval("require slutils;", [])
        (lastSession, sessions) = MuSymbol('slutils.retrieveSessionsData')()
        return lastSession.split('|')

    def showdialog(self):
        self.publish_dialog = QtGui.QDialog()

        #b1.clicked.connect(self.exit_dialog)
        self.publish_dialog.setWindowTitle("Publish Settings")
        self.publish_dialog.setMinimumSize(QtCore.QSize(250, 208))
        self.publish_dialog.setMaximumSize(QtCore.QSize(260, 208))
        self.publish_dialog.setWindowModality(QtCore.Qt.ApplicationModal)

        layout = QtGui.QVBoxLayout(self.publish_dialog)
        b1 = QtGui.QPushButton("Publish")
        b2 = QtGui.QPushButton("Cancel")
        # b1.move(50, 50)
        val = "hello Deepak!"
        self.publish_dialog.buttons = QtGui.QDialogButtonBox()
        self.publish_dialog.buttons.addButton(b1, QtGui.QDialogButtonBox.AcceptRole)
        self.publish_dialog.buttons.addButton(b2, QtGui.QDialogButtonBox.RejectRole)
        self.publish_dialog.buttons.accepted.connect(partial(self.exit_dialog, val))
        self.publish_dialog.buttons.rejected.connect(self.publish_dialog.reject)
        layout.addWidget(self.publish_dialog.buttons)

        self.publish_dialog.exec_()

        return val

    def exit_dialog(self, val):
        print "exiting.. " + val
        self.publish_dialog.close()

    def _toggle_floating(self):
        """
        Toggles the parent dock widget's floating status.
        """
        if self.dock:
            if self.dock.isFloating():
                self.dock.setFloating(False)
                self.dock()
            else:
                self.dock.setFloating(True)

    def _on_float_changed(self):
        if self.dock:
            if self.dock.isFloating():
                self.dock.setTitleBarWidget(None)
            else:
                self.dock.setTitleBarWidget(self._dock_title)

    def toggle_show_shot_name(self):
        self.ui.cut_lst.toggle_show_shot_names()
