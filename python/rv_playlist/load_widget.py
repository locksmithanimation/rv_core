import operator

from sequence_tools.sequence_objects import Shot, Take
from .ui.load_widget import Ui_LoadWidget
from tank.platform.qt import QtCore, QtGui
from .caches import ShotgunCaches
from .shot_widget import ShotWidget

class LoadWidget(QtGui.QFrame):
    def __init__(self, list, dialog, list_item):
        self.entity_type = "load"
        self.dialog = dialog
        self.parent = list
        self.list_item = list_item
        self.seqout = None
        self.order = None

        QtGui.QFrame.__init__(self)

        self.ui = Ui_LoadWidget()
        self.ui.setupUi(self)
        self.ui.load_bar.setVisible(False)
        take_completer_list = [x["code"] for x in ShotgunCaches.TAKE_CACHE]
        take_completer = QtGui.QCompleter(take_completer_list, self)
        take_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        completer_font = take_completer.popup().font()
        self.font_metrics = QtGui.QFontMetrics(completer_font)
        for take in take_completer_list:
            item_width = self.font_metrics.width(take) + 50
            current_width = take_completer.popup().minimumWidth()
            if item_width > current_width:
                take_completer.popup().setMinimumWidth(item_width)
        self.ui.take_box.setCompleter(take_completer)
        cut_completer_list = [x["code"] for x in ShotgunCaches.CUT_CACHE]
        cut_completer = QtGui.QCompleter(cut_completer_list, self)
        cut_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        for cut in cut_completer_list:
            item_width = self.font_metrics.width(cut) + 50
            current_width = cut_completer.popup().minimumWidth()
            if item_width > current_width:
                cut_completer.popup().setMinimumWidth(item_width)
        self.ui.cut_box.setCompleter(cut_completer)
        self.ui.take_box.editingFinished.connect(self._on_take_finished)
        self.ui.cut_box.editingFinished.connect(self._on_cut_finished)

    def update_item(self, list, list_item):
        self.list_item = list_item
        self.parent = list

    def _on_cut_finished(self):
        if self.ui.cut_box.hasFocus():
            self.ui.cut_box.clearFocus()
            return
        self.cut_from_name(self.ui.cut_box.text())

    def _on_take_finished(self):
        if self.ui.take_box.hasFocus():
            self.ui.take_box.clearFocus()
            return
        self.take_from_name(self.ui.take_box.text())

    def cut_from_name(self, name):
        if not name:
            return
        self.menu_bar_visible(True)
        cut = next((x for x in ShotgunCaches.CUT_CACHE if x["code"] == name), None)
        if cut:
            update_value = 100 / (len(cut["cut_items"]) + 1)
            cut_items = ShotgunCaches.SHOTGUN.find("CutItem",
                                                   [["project", "is", {"type": "Project", "id": 158}],
                                                    ["cut", "is", cut]],
                                                   ["code", "cut_item_in", "cut_item_out", "shot", "cut_order",
                                                    "edit_in", "edit_out", "sg_take", "version", "sg_original_media",
                                                    "version.Version.entity", "sg_track"])
            self.ui.load_bar.setValue(update_value)
            for cut_item in sorted(cut_items, key=operator.itemgetter("edit_in")):
                widget = ShotWidget(self.parent, self.dialog)
                item, widget = self.parent.add_item(shot_widget=widget)
                widget.list_item = item
                widget.shot_from_cut_item(cut_item)
                self.ui.load_bar.setValue(self.ui.load_bar.value() + update_value)
                widget.shot_changed.connect(self.parent.set_edl)
            self.parent.set_edl()
            self.parent.takeItem(self.parent.row(self.list_item))
        else:
            self.menu_bar_visible(False)

    def take_from_name(self, name):
        take = next((x for x in ShotgunCaches.TAKE_CACHE if x["code"] == name), None)
        if take:
            new_widget = ShotWidget(self.parent, self.dialog, self.list_item)
            self.parent.replace_widget(self.list_item, new_widget)
            new_widget.shot_from_take(take)

    def menu_bar_visible(self, visible):
        self.ui.load_bar.setVisible(visible)
        self.ui.cut_box.setVisible(not visible)
        self.ui.cut_lbl.setVisible(not visible)
        self.ui.take_box.setVisible(not visible)
        self.ui.take_lbl.setVisible(not visible)
