import operator
import getpass
import os

from sequence_tools.sequence_objects import Shot, Take
from sequence_tools.publish_objects import PublishTake
from .ui.shot_widget import Ui_ShotWidget
from tank.platform.qt import QtCore, QtGui
from .caches import ShotgunCaches

import rv.commands as rvc

class ShotWidget(QtGui.QFrame):
    PLAYBLAST_PATH = "//hal/WIP/PROJECTS/BOT/DEPARTMENTS/PREVIS/Playblasts"
    shot_changed = QtCore.Signal()

    def __init__(self, list, dialog, list_item=None):
        self.entity_type = "shot"
        self.name = None
        self.parent = list
        self.list_item = list_item
        self.shot = None
        self.sequence_start = None
        self.shotin = 0
        self.shotout = 0
        self.order = None
        self.original_version = None
        self.current_version = None
        self.versions = []
        self.source_group = None
        self.file_source = None
        self.overlay = None
        self.audio = None

        QtGui.QFrame.__init__(self)

        self.ui = Ui_ShotWidget()
        self.ui.setupUi(self)

        combo_font = self.ui.version_cmb.font()
        self.font_metrics = QtGui.QFontMetrics(combo_font)

        take_completer_list = [x["code"] for x in ShotgunCaches.TAKE_CACHE]
        take_completer = QtGui.QCompleter(take_completer_list, self)
        #self.ui.take_box.setCompleter(take_completer)

        self.ui.version_cmb.currentIndexChanged.connect(self._on_version_changed)
        self.ui.name_box.editingFinished.connect(self._on_name_finished)
        self.ui.shotin_box.editingFinished.connect(self._on_shotin_finished)
        self.ui.shotout_box.editingFinished.connect(self._on_shotout_finished)
        self.ui.enabled_chk.stateChanged.connect(self._on_enabled_changed)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            super(ShotWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        super(ShotWidget, self).dragMoveEvent(event)
        pos = self.mapFromGlobal(QtGui.QCursor.pos())
        stylesheet = self.base_stylesheet()
        if self.list_item.isSelected():
            dark = SELECTED_DARK
            light = SELECTED_LIGHT
        else:
            dark = DESELECTED_DARK
            light = DESELECTED_LIGHT
        if pos.y() < 9:
            stylesheet += "QFrame#ShotWidget{border-top-width:2px;\n" \
                          "                  border-top-color:blue;\n" \
                          "                  border-top-style:solid;}\n"
        elif pos.y() > 24:
            stylesheet += "QFrame#ShotWidget{border-bottom-width:2px;\n" \
                          "                  border-bottom-color:blue;\n" \
                          "                  border-bottom-style:solid;}\n"
        else:
            stylesheet += "QFrame#ShotWidget{border-width:2px;\n" \
                          "                  border-color:blue;\n" \
                          "                  border-style:solid;}\n"
        self.setStyleSheet(stylesheet)

    def dragLeaveEvent(self, event):
        super(ShotWidget, self).dragLeaveEvent(event)
        self.reset_stylesheet()

    def dropEvent(self, event):
        self.reset_stylesheet()
        pos = self.mapFromGlobal(QtGui.QCursor.pos())
        if pos.y() < 7 or pos.y() > 25:
            self.parent.dropEvent(event)
        else:
            mime_path = [x.toLocalFile() for x in event.mimeData().urls()][0]
            if not mime_path.endswith("mov"):
                return
            take = self.parent.load_media_file(mime_path)
            self.update_take(take)

    def update_take(self, take):
            if take["type"] == "CutItem":
                self.shot_from_cut_item(take)
            else:
                self.shot_from_take(take)

    def base_stylesheet(self):
        if self.list_item.isSelected():
            stylesheet = LOADED_SELECTED
        else:
            stylesheet = LOADED_DESELECTED
        return stylesheet

    def reset_stylesheet(self):
        stylesheet = self.base_stylesheet()
        self.setStyleSheet(stylesheet)

    def take_from_name(self, name):
        take = next((x for x in ShotgunCaches.TAKE_CACHE if x["code"] == name), None)
        if take:
            self.shot_from_take(take)

    def _on_name_finished(self):
        if self.ui.name_box.hasFocus():
            self.ui.name_box.clearFocus()
            return
        new_name = self.ui.name_box.text()
        if self.name and self.name in self.parent.shot_list:
            self.parent.shot_list.remove(self.name)
        self.name = new_name
        if new_name:
            self.parent.shot_list.append(new_name)
        if self.shot.name and self.shot.name != new_name:
            self.shot.name = None
        self.create_text(self.overlay + ".text:shot", self.ui.name_box.text(), 0, -0.43)

    def _on_shotin_finished(self):
        if self.ui.shotin_box.hasFocus():
            self.ui.shotin_box.clearFocus()
            return
        self.shotin = self.ui.shotin_box.value()
        self.shot_changed.emit()

    def _on_shotout_finished(self):
        if self.ui.shotout_box.hasFocus():
            self.ui.shotout_box.clearFocus()
            return
        self.shotout = self.ui.shotout_box.value()
        self.shot_changed.emit()

    def _on_enabled_changed(self, state):
        self.ui.order_lbl.setText("")
        self.shot_changed.emit()

    def set_shot(self, shot, versions):
        self.shot = shot
        self.activate_shot(versions)

    def shot_from_take(self, take_dict):
        if not take_dict:
            return
        shot = Shot()
        take = PublishTake.take_from_sg(take_dict)
        shot.take = take
        self.shot = shot
        versions = None
        if "file_versions" in take_dict:
            versions = take_dict["file_versions"]
        self.activate_shot(versions=versions)
        self.shotin = self.ui.shotin_box.value()
        self.shotout = self.ui.shotout_box.value()
        if self.shot.sequence_in:
            self.sequence_start = self.shot.sequence_in

    def shot_from_cut_item(self, cut_item_dict):
        if not cut_item_dict:
            return
        if "audio" in cut_item_dict:
            self.audio = cut_item_dict["audio"]
        shot = Shot.shot_from_cut_item(cut_item_dict)
        take_dict = None
        take_name = None
        if cut_item_dict["sg_take"]:
            take_name = cut_item_dict["sg_take"]["name"]
        elif cut_item_dict["version"]:
            version_name = cut_item_dict["version"]["name"]
        if take_name:
            take_dict = next((x for x in ShotgunCaches.TAKE_CACHE if x["code"] == take_name), None)
        elif version_name:
            take_dict = None
            for item in ShotgunCaches.TAKE_CACHE:
                for version in item["sg_versions"]:
                    if version["name"] == take_name:
                        take_dict = item
                        break
                if take_dict:
                    break
        if not take_dict and (cut_item_dict["sg_take"] or cut_item_dict["version"]):
            take_dict = cut_item_dict["version"]
        if "version.Version.entity" in cut_item_dict:
            take_dict["entity"] = cut_item_dict["version.Version.entity"]
        if take_dict:
            take = PublishTake.take_from_sg(take_dict)
        else:
            take = PublishTake()
            take.name = shot.name
            take.take_in = shot.shot_in
            take.take_out = shot.shot_out
            take.shotgun_entity = cut_item_dict
        self.original_version = cut_item_dict["sg_original_media"] or cut_item_dict["version"]
        shot.take = take
        self.shot = shot
        versions = None
        if "file_versions" in take_dict:
            versions = take_dict["file_versions"]
        self.activate_shot(versions=versions)
        self.shotin = self.ui.shotin_box.value()
        self.shotout = self.ui.shotout_box.value()
        if self.shot.sequence_in:
            self.sequence_start = self.shot.sequence_in

    def load_media(self):
        if not self.shot.take.temp_path:
            return
        self.file_source = None
        if rvc.sources() and self.shot.take.temp_path.replace("\\", "/") in [x[0] for x in rvc.sources()]:
            for file_source in rvc.nodesOfType("RVFileSource"):
                if rvc.sourceMedia(file_source)[0] == self.shot.take.temp_path.replace("\\", "/"):
                    self.file_source = file_source
                    self.source_group = rvc.nodeGroup(self.file_source)
        if not self.file_source:
            sources = [self.shot.take.temp_path]
            sequence_media_start = self.shot.sequence_in - (self.shot.shot_in - self.shot.take.take_in)
            if not self.shot.take.audio and self.audio:
                self.shot.take.audio = self.audio["path"]
            if not self.shot.take.audio_offset and self.audio:
                self.shot.take.audio_offset = (self.audio["offset"] - sequence_media_start)
            if self.shot.take.audio:
                sources.append(self.shot.take.audio)
            self.file_source = rvc.addSourceVerbose(sources)
            self.source_group = rvc.nodeGroup(self.file_source)
            source_start = rvc.sourceMediaInfo(self.file_source).get("startFrame")
            prop = self.file_source + ".group.rangeOffset"
            offset = self.shot.take.take_in - source_start
            if self.shot.take.audio:
                rvc.setFloatProperty(self.file_source + ".group.audioOffset", [self.shot.take.audio_offset / 24.0], True)
            rvc.setIntProperty(prop, [offset], True)
        current_shots = rvc.nodeConnections("Playlist") or []
        current_shots = current_shots and current_shots[0]
        if self.source_group not in current_shots:
            rvc.setNodeInputs("Playlist", current_shots + [self.source_group])
        self.overlay = self.follow_graph(self.file_source, 1, 6)
        self.update_cut_info()
        self.shot_changed.emit()

    def follow_graph(self, node, direction, distance):
        if distance:
            connection = rvc.nodeConnections(node)[direction][0]
            if distance > 1:
                connection = self.follow_graph(connection, direction, distance - 1)
        return connection

    def _on_version_changed(self):
        self.current_version = self.ui.version_cmb.itemData(self.ui.version_cmb.currentIndex())
        if not self.current_version:
            return
        new_take_in = self.current_version["sg_first_frame"]
        new_take_out = self.current_version["sg_last_frame"]

        self.shot.take.take_in = new_take_in
        self.shot.take.take_out = new_take_out
        if not self.shot.shot_in:
            self.shot.shot_in = new_take_in
        if not self.shot.shot_out:
            self.shot.shot_out = new_take_out
        self.shot.take.temp_path = self.current_version["sg_path_to_movie"] or self.current_version["sg_path_to_frames"]
        self.load_media()

    def activate_shot(self, latest=False, versions=None):
        if not self.shot:
            return
        entity = None
        if self.shot.take:
            entity = self.shot.take.shotgun_entity
            if entity["type"] == "Version":
                entity = entity["entity"]
        elif self.shot:
            entity = self.shot.shotgun_entity
        if versions:
            self.versions = versions
        else:
            self.versions = ShotgunCaches.SHOTGUN.find("Version",
                                                       [["project", "is", {"type": "Project", "id": 158}],
                                                        ["entity", "is", entity]],
                                                       ["code", "sg_first_frame", "sg_last_frame",
                                                        "sg_path_to_movie", "sg_path_to_frames"])
            self.versions = sorted(self.versions, key=operator.itemgetter("code"), reverse=True)

        self.blockSignals(True)
        self.ui.version_cmb.blockSignals(True)
        self.ui.version_cmb.clear()
        for version in self.versions:
            item_width = self.font_metrics.width(version["code"]) + 60
            current_width = self.ui.version_cmb.view().minimumWidth()
            if item_width > current_width:
                self.ui.version_cmb.view().setMinimumWidth(item_width)
            self.ui.version_cmb.addItem(version["code"], version)

        if latest:
            self.ui.version_cmb.setCurrentIndex(0)
        elif self.original_version:
            for ii in range(self.ui.version_cmb.count()):
                if self.ui.version_cmb.itemData(ii)["id"] == self.original_version["id"]:
                    self.ui.version_cmb.setCurrentIndex(ii)
                    break
        self._on_version_changed()
        self.blockSignals(False)
        self.ui.version_cmb.blockSignals(False)
        self.shot_changed.emit()
        self.setStyleSheet(LOADED_DESELECTED)

    def set_version(self, version_number):
        for ii in range(self.ui.version_cmb.count()):
            if self.ui.version_cmb.itemText(ii).endswith(version_number):
                self.ui.version_cmb.setCurrentIndex(ii)
                break

    def update_cut_info(self):
        if not self.shot:
            return
        self.ui.name_box.setText("")
        self.name = None
        self.ui.shotin_box.setValue(self.shot.shot_in or self.shot.take.take_in or 0)
        self._on_shotin_finished()
        self.ui.shotout_box.setValue(self.shot.shot_out or self.shot.take.take_out or 0)
        self._on_shotout_finished()

        index = self.parent.indexFromItem(self.list_item).row()
        name = self.shot.name
        """
        if not name:
            shot_number = (index + 1) * 10
            name = "{}_pv{:04}".format(self.shot.take.sequence, shot_number)
            while name in self.parent.shot_list:
                shot_number += 10
                name = "{}_pv{:04}".format(self.shot.take.sequence, shot_number)
        """
        self.ui.name_box.setText(name)
        self._on_name_finished()

        self.ui.order_lbl.setText(str(index))

    def update_local(self, user=None):
        if not user:
            user = getpass.getuser()
        local_path = os.path.join(self.PLAYBLAST_PATH, user)
        session_dirs = os.listdir(local_path)
        session_dirs = sorted(session_dirs, reverse=True)
        for session_dir in session_dirs:
            session_path = os.path.join(local_path, session_dir)
            if os.path.isdir(session_path):
                take_dirs = os.listdir(session_path)
                for take_dir in take_dirs:
                    take_path = os.path.join(session_path, take_dir)
                    if os.path.isdir(take_path) and take_dir == self.shot.take.name:
                        movie_file = next((x for x in os.listdir(take_path) if x.endswith("mov")), None)
                        if movie_file:
                            movie_path = os.path.join(take_path, movie_file)
                            take = self.parent.load_media_file(movie_path)
                            self.update_take(take)
                            return

    def set_quicktime(self):
        files = rvc.getStringProperty("{}.media.movie".format(self.file_source))
        if files[0].endswith(".mov"):
            return
        mov = sorted([x for x in os.listdir(os.path.dirname(files[0])) if x.endswith(".mov")])[-1]
        if mov:
            files[0] = "/".join([os.path.dirname(files[0]), mov])
            rvc.setStringProperty("{}.media.movie".format(self.file_source), files, True)
            rvc.setIntProperty("{}.group.rangeOffset".format(self.file_source), [self.shot.take.take_in], True)
            rvc.setIntProperty("{}.cut.in".format(self.file_source), [-2147483647], True)
            rvc.setIntProperty("{}.cut.out".format(self.file_source), [2147483647], True)

    def set_frames(self):
        files = rvc.getStringProperty("{}.media.movie".format(self.file_source))
        if files[0].endswith(".jpg"):
            return
        jpg = next((x for x in os.listdir(os.path.dirname(files[0])) if x.endswith(".jpg")))
        if jpg:
            jpg_parts = jpg.split(".")
            jpg_parts[-2] = "#"
            jpg = ".".join(jpg_parts)
            files[0] = "/".join([os.path.dirname(files[0]), jpg])
            rvc.setStringProperty("{}.media.movie".format(self.file_source), files, True)
            rvc.setIntProperty("{}.group.rangeOffset".format(self.file_source), [0], True)
            rvc.setIntProperty("{}.cut.in".format(self.file_source), [self.shot.take.take_in], True)
            rvc.setIntProperty("{}.cut.out".format(self.file_source), [self.shot.take.take_out], True)

    def create_text(self, node, value, hpos, vpos):
        rvc.newProperty('%s.position' % node, rvc.FloatType, 2)
        rvc.newProperty('%s.color' % node, rvc.FloatType, 4)
        rvc.newProperty('%s.spacing' % node, rvc.FloatType, 1)
        rvc.newProperty('%s.size' % node, rvc.FloatType, 1)
        rvc.newProperty('%s.scale' % node, rvc.FloatType, 1)
        rvc.newProperty('%s.rotation' % node, rvc.FloatType, 1)
        rvc.newProperty("%s.font" % node, rvc.StringType, 1)
        rvc.newProperty("%s.text" % node, rvc.StringType, 1)
        rvc.newProperty("%s.origin" % node, rvc.StringType, 1)
        rvc.newProperty('%s.debug' % node, rvc.IntType, 1)

        rvc.setFloatProperty('%s.position' % node, [float(hpos), float(vpos)], True)
        rvc.setFloatProperty('%s.color' % node, [0.0, 1.0, 0.0, 1.0], True)
        rvc.setFloatProperty('%s.spacing' % node, [1.0], True)
        rvc.setFloatProperty('%s.size' % node, [0.002], True)
        rvc.setFloatProperty('%s.scale' % node, [1.0], True)
        rvc.setFloatProperty('%s.rotation' % node, [0.0], True)
        rvc.setStringProperty("%s.font" % node, [""], True)
        rvc.setStringProperty("%s.text" % node, [value], True)
        rvc.setStringProperty("%s.origin" % node, ["center-center"], True)
        rvc.setIntProperty('%s.debug' % node, [0], True)

    def contextMenuEvent(self, event):
        users = [x for x in os.listdir(self.PLAYBLAST_PATH) if os.path.isdir(os.path.join(self.PLAYBLAST_PATH, x))]
        selection = self.parent.selectedItems()
        menu = QtGui.QMenu(self)
        delete_data = menu.addAction("Delete")
        local_data = menu.addAction("Latest Local")
        user_data = menu.addMenu("Latest Local by User")
        user_actions = {}
        for user in users:
            user_actions[user] = user_data.addAction(user)
        published_data = menu.addAction("Latest Published")
        quicktime = menu.addAction("Switch to Quicktime")
        frames = menu.addAction("Switch to Frames")
        action = menu.exec_(self.mapToGlobal(event.pos()))
        if action == delete_data:
            self.parent.remove_item([self.list_item])
        elif action == local_data:
            self.parent.update_local(selection)
        elif action == published_data:
            self.shot.shot_in = None
            self.shot.shot_out = None
            self.activate_shot(latest=True)
        elif action == quicktime:
            self.set_quicktime()
        elif action == frames:
            self.set_frames()
        else:
            for user in user_actions:
                if action == user_actions[user]:
                    self.parent.update_local(selection, user)


BASE_STYLESHEET = "QLineEdit{background-color:rgb(255, 255, 255, 0);\n" \
                  "                color:black;}\n" \
                  "QComboBox{background-color:rgb(255, 255, 255, 0);\n" \
                  "                     color:black;}\n" \
                  "QComboBox:focus{color:black;}\n" \
                  "QComboBox QAbstractItemView{color:black;}\n" \
                  "QLabel{color:black;}\n" \
                  "QSpinBox{background-color:rgb(255, 255, 255, 0);\n" \
                  "                 color:black;}"

DESELECTED_DARK = "rgb(76, 111, 135)"
DESELECTED_LIGHT = "rgb(132, 191, 233)"
SELECTED_DARK = "rgb(170, 170, 85)"
SELECTED_LIGHT = "rgb(255, 255, 166)"

LOADED_SELECTED = "QFrame{{background-color:rgb(255, 255, 128)}}\n{base}\n" \
                  "QFrame#ShotWidget{{border-width:1px;\n" \
                  "                  border-style:solid;\n" \
                  "                  border-bottom-color:{dark};\n" \
                  "                  border-top-color:{light};\n" \
                  "                  border-left-color:{light};\n" \
                  "                  border-right-color:{dark};}}\n".format(base=BASE_STYLESHEET,
                                                                            light=SELECTED_LIGHT,
                                                                            dark=SELECTED_DARK)

LOADED_DESELECTED = "QFrame{{background-color:rgb(115, 166, 202)}}\n{base}\n" \
                    "QFrame#ShotWidget{{border-width:1px;\n" \
                    "                  border-style:solid;\n" \
                    "                  border-bottom-color:{dark};\n" \
                    "                  border-top-color:{light};\n" \
                    "                  border-left-color:{light};\n" \
                    "                  border-right-color:{dark};}}\n".format(base=BASE_STYLESHEET,
                                                                              light=DESELECTED_LIGHT,
                                                                              dark=DESELECTED_DARK)
