# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'W:\DEPARTMENTS\DEV\willw\packages\rv\python\rv_playlist\resources\load_widget.ui'
#
# Created: Tue Jan 15 13:08:29 2019
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

class Ui_LoadWidget(object):
    def setupUi(self, LoadWidget):
        LoadWidget.setObjectName("LoadWidget")
        LoadWidget.resize(317, 34)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(LoadWidget.sizePolicy().hasHeightForWidth())
        LoadWidget.setSizePolicy(sizePolicy)
        LoadWidget.setMinimumSize(QtCore.QSize(150, 0))
        LoadWidget.setMaximumSize(QtCore.QSize(16000, 34))
        LoadWidget.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        LoadWidget.setStyleSheet("QFrame{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(103, 103, 103, 255), stop:1 rgba(92, 92, 92, 255));}\n"
"QLineEdit{background-color:rgb(255, 255, 255, 0);\n"
"                color:black;}\n"
"QComboBox{background-color:rgb(255, 255, 255, 0);\n"
"                     color:black;}\n"
"QComboBox:focus{color:black;}\n"
"QComboBox QAbstractItemView{color:black;}\n"
"QLabel{color:black;}\n"
"QSpinBox{background-color:rgb(255, 255, 255, 0);\n"
"                 color:black;}")
        LoadWidget.setFrameShape(QtGui.QFrame.Panel)
        LoadWidget.setFrameShadow(QtGui.QFrame.Raised)
        LoadWidget.setLineWidth(1)
        self.verticalLayout_2 = QtGui.QVBoxLayout(LoadWidget)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.cut_shot_lay = QtGui.QHBoxLayout()
        self.cut_shot_lay.setSpacing(0)
        self.cut_shot_lay.setContentsMargins(5, 5, 5, 5)
        self.cut_shot_lay.setObjectName("cut_shot_lay")
        self.take_lbl = QtGui.QLabel(LoadWidget)
        self.take_lbl.setMaximumSize(QtCore.QSize(30, 16777215))
        self.take_lbl.setObjectName("take_lbl")
        self.cut_shot_lay.addWidget(self.take_lbl)
        self.take_box = QtGui.QLineEdit(LoadWidget)
        self.take_box.setMinimumSize(QtCore.QSize(50, 18))
        self.take_box.setMaximumSize(QtCore.QSize(200, 18))
        font = QtGui.QFont()
        font.setFamily("MS UI Gothic")
        font.setPointSize(7)
        self.take_box.setFont(font)
        self.take_box.setStyleSheet("")
        self.take_box.setText("")
        self.take_box.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.take_box.setObjectName("take_box")
        self.cut_shot_lay.addWidget(self.take_box)
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.cut_shot_lay.addItem(spacerItem)
        self.cut_lbl = QtGui.QLabel(LoadWidget)
        self.cut_lbl.setMinimumSize(QtCore.QSize(0, 0))
        self.cut_lbl.setMaximumSize(QtCore.QSize(25, 16777215))
        self.cut_lbl.setObjectName("cut_lbl")
        self.cut_shot_lay.addWidget(self.cut_lbl)
        self.cut_box = QtGui.QLineEdit(LoadWidget)
        self.cut_box.setMinimumSize(QtCore.QSize(50, 18))
        self.cut_box.setMaximumSize(QtCore.QSize(200, 18))
        font = QtGui.QFont()
        font.setFamily("MS UI Gothic")
        font.setPointSize(7)
        self.cut_box.setFont(font)
        self.cut_box.setStyleSheet("")
        self.cut_box.setText("")
        self.cut_box.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.cut_box.setObjectName("cut_box")
        self.cut_shot_lay.addWidget(self.cut_box)
        spacerItem1 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.cut_shot_lay.addItem(spacerItem1)
        self.verticalLayout_2.addLayout(self.cut_shot_lay)
        self.load_bar = QtGui.QProgressBar(LoadWidget)
        self.load_bar.setProperty("value", 0)
        self.load_bar.setAlignment(QtCore.Qt.AlignCenter)
        self.load_bar.setTextVisible(True)
        self.load_bar.setOrientation(QtCore.Qt.Horizontal)
        self.load_bar.setObjectName("load_bar")
        self.verticalLayout_2.addWidget(self.load_bar)

        self.retranslateUi(LoadWidget)
        QtCore.QMetaObject.connectSlotsByName(LoadWidget)

    def retranslateUi(self, LoadWidget):
        LoadWidget.setWindowTitle(QtGui.QApplication.translate("LoadWidget", "Frame", None, QtGui.QApplication.UnicodeUTF8))
        self.take_lbl.setText(QtGui.QApplication.translate("LoadWidget", "Take: ", None, QtGui.QApplication.UnicodeUTF8))
        self.cut_lbl.setText(QtGui.QApplication.translate("LoadWidget", "Cut: ", None, QtGui.QApplication.UnicodeUTF8))

