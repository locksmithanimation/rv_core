import os
import sys
import json
import operator

from .caches import ShotgunCaches
from . import shot_widget
from .load_widget import LoadWidget
from .shot_widget import ShotWidget
from edit_tools import import_essence
from edit_tools.cut_data import CutData
from tank.platform.qt import QtCore, QtGui
import rv.commands as rvc

drives = [{'remote': u'\\\\hal\\software', 'local': u'S:'},
          {'remote': u'\\\\hal\\flixNew', 'local': u'T:'},
          {'remote': u'\\\\hal\\WIP', 'local': u'W:'},
          {'remote': u'\\\\hal\\data', 'local': u'Z:'},
          {'remote': u'\\\\lsastore01\\office', 'local': u'Q:'}]

class CutList(QtGui.QListWidget):

    content_changed = QtCore.Signal()

    def __init__(self, parent):
        super(CutList, self).__init__(parent)
        self.types = ["jpg", "jpeg", "png", "mov"]
        self._dialog = None
        self.itemSelectionChanged.connect(self._on_selection_change)
        self.shot_list = []
        self.drag_widgets = None
        self.order_start = 0
        self.current_shot = None
        self.sequence = None
        self.show_shot_names = True
        self.drives = None
        if "Playlist_sequence" not in rvc.nodesOfType("RVSequence"):
            self.sequence_node = rvc.newNode("RVSequenceGroup", "Playlist")
        else:
            self.load_from_rv()
        rvc.setViewNode("Playlist")
        rvc.bind("default", "global", "frame-changed", self.update_overlay, "")

    @property
    def dialog(self):
        return self._dialog

    @dialog.setter
    def dialog(self, value):
        self._dialog = value

    @property
    def cut(self):
        if self.count() < 1:
            return None
        shots = []
        takes = {}
        start_frame = 1001
        current_start = None
        for ii in range(self.count()):
            item = self.item(ii)
            widget = self.itemWidget(item)
            if not widget.ui.enabled_chk.isChecked():
                continue
            if not current_start:
                if not widget.sequence_start:
                    current_start = 1001
                else:
                    start_frame = widget.sequence_start + widget.ui.shotin_box.value()
                    current_start = start_frame
            shots.append(widget.shot)
            widget.shot.order = ii + 1
            duration = widget.shot.duration
            widget.shot.sequence_in = current_start
            widget.shot.sequence_out = current_start + duration - 1
            widget.shot.shot_in = widget.shotin
            widget.shot.shot_out = widget.shotout
            temp_path = widget.shot.take.temp_path
            if temp_path not in takes:
                takes[temp_path] = widget.shot.take
            else:
                widget.shot.take = takes[temp_path]
            current_start += duration
        sequence = None
        if self.sequence and self.sequence != "mixed":
            sequence = self.sequence
        else:
            for take in takes.values():
                if take.sequence:
                    sequence = take.sequence
                    break
        cut = CutData(sequence=sequence, shots=shots, takes=takes.values(), start_frame=start_frame)
        return cut

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            super(CutList, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        super(CutList, self).dragMoveEvent(event)

    def dropEvent(self, event):
        if not event.mimeData().hasUrls():
            super(CutList, self).dropEvent(event)
            return

        mime_paths = [x.toLocalFile() for x in event.mimeData().urls()]
        for mime_path in mime_paths:
            for drive_map in drives:
                if mime_path.startswith(drive_map["local"]):
                    mime_path = mime_path.replace(drive_map["local"], drive_map["remote"])
                    break
            self.load_from_file(path=mime_path)

    def load_from_file(self, path=None, json_obj=None):
        cut_items = []
        takes = []
        if path:
            if path.endswith("mov"):
                item = self.load_media_file(path)
                if item["type"] == "CutItem":
                    cut_items.append(item)
                else:
                    takes.append(item)
            if path.endswith("json"):
                cut_items.extend(self.cut_from_json(path=path))
        elif json_obj:
            cut_items = self.cut_from_json(json_obj=json_obj)
        if cut_items:
            cut_items = sorted(cut_items, key=operator.itemgetter("cut_order"))
            for cut_item in cut_items:
                new_widget = ShotWidget(self, self.dialog, None)
                new_item, new_widget = self.add_item(shot_widget=new_widget)
                new_widget.list_item = new_item
                new_widget.shot_changed.connect(self.set_edl)
                new_widget.shot_from_cut_item(cut_item)
                new_widget.ui.shotin_box.setValue(cut_item["cut_item_in"])
                new_widget._on_shotin_finished()
                new_widget.ui.shotout_box.setValue(cut_item["cut_item_out"])
                new_widget._on_shotout_finished()
        if takes:
            takes = sorted(takes, key=operator.itemgetter("take_in"))
            for take in takes:
                new_widget = ShotWidget(self, self.dialog, None)
                new_item, new_widget = self.add_item(shot_widget=new_widget)
                new_widget.list_item = new_item
                new_widget.shot_changed.connect(self.set_edl)
                new_widget.shot_from_take(take["take"])
                if take["version"]:
                    new_widget.set_version(take["version"])

    def load_media_file(self, path):
        cut_path = os.path.join(os.path.dirname(os.path.dirname(path)), "cut.json")
        json_data = None
        if os.path.exists(cut_path):
            with open(cut_path, "r") as json_file:
                json_data = json.load(json_file)
        file_name = os.path.basename(path)
        try:
            take_name, version, extension = file_name.split(".")
        except ValueError:
            take_name, extension = file_name.split(".")
            version = None
        except ValueError:
            take_name = file_name.split(".")
            version = None
            extension = None
        cut_data = None
        sequence = None
        audio = None
        if json_data:
            sequence = json_data["sequence"]
            for take_data in json_data["takes"]:
                if take_data["name"] == take_name:
                    audio = json_data["audio"]
                    cut_data = take_data
        take = next((x for x in ShotgunCaches.TAKE_CACHE if x["code"] == take_name), None)
        if not take:
            take = {"code": take_name,
                    "type": "CustomEntity06",
                    "id": 0}
        take["name"] = take["code"]
        if cut_data:
            in_frame = cut_data["take_in"]
            out_frame = cut_data["take_out"]
        else:
            media_data = import_essence.probe(path)
            in_frame = 0
            out_frame = int(round(float(media_data["format"]["duration"]) * 24))
        take["sg_take_in"] = in_frame
        take["sg_take_out"] = out_frame
        take["file_versions"] = [{"code": file_name,
                                  "id": 0,
                                  "sg_first_frame": in_frame,
                                  "sg_last_frame": out_frame,
                                  "sg_path_to_movie": path,
                                  "sg_path_to_frames": None}]
        if sequence:
            take["sg_sequence"] = sequence
        cut_item = None
        if cut_data and cut_data["shots"]:
            shots = []
            for shot_data in json_data["shots"]:
                if shot_data["name"] in cut_data["shots"]:
                    shots.append(shot_data)
            for shot in shots:
                cut_item = {"id": 0,
                            "type": "CutItem",
                            "code": shot["name"],
                            "cut_item_in": shot["shot_in"],
                            "cut_item_out": shot["shot_out"],
                            "shot": None,
                            "cut_order": shot["order"],
                            "edit_in": shot["sequence_in"],
                            "edit_out": shot["sequence_out"],
                            "sg_take": None,
                            "version": take,
                            "sg_original_media": None,
                            "audio": audio}
        if not cut_item:
            cut_item = {"id": 0,
                        "type": "CutItem",
                        "code": take["name"],
                        "cut_item_in": take["sg_take_in"],
                        "cut_item_out": take["sg_take_out"],
                        "shot": None,
                        "cut_order": 0,
                        "edit_in": take["sg_take_in"],
                        "edit_out": take["sg_take_out"],
                        "sg_take": None,
                        "version": take,
                        "sg_original_media": None,
                        "audio": audio}
        return cut_item

    def load_from_rv(self):
        cut_items = []
        frames = rvc.getIntProperty("Playlist_sequence.edl.frame")[:-1]
        indexes = rvc.getIntProperty("Playlist_sequence.edl.source")[:-1]
        ins = rvc.getIntProperty("Playlist_sequence.edl.in")[:-1]
        outs = rvc.getIntProperty("Playlist_sequence.edl.out")[:-1]
        unique_indexes = list(set(indexes))
        groups = rvc.nodesOfType("RVSourceGroup")
        sources = rvc.nodesOfType("RVFileSource")
        takes = {}
        for this_index in unique_indexes:
            take = {}
            source = sources[this_index]
            group = groups[this_index]
            take["name"] = rvc.getStringProperty("{}.ui.name".format(group))[0]
            take["code"] = take["name"]
            take["type"] = "CustomEntity06"
            take["id"] = 0
            take["file_versions"] = [{"id": 0}]
            cut_in = rvc.getIntProperty("{}.cut.in".format(source))[0]
            media = rvc.getStringProperty("{}.media.movie".format(source))[0]
            if cut_in > 0:
                take["take_in"] = cut_in
                take["take_out"] = rvc.getIntProperty("{}.cut.out".format(source))[0]
                take["file_versions"][0]["sg_path_to_frames"] = media
                take["file_versions"][0]["sg_path_to_movie"] = None
            else:
                take["file_versions"][0]["sg_path_to_movie"] = media
                take["file_versions"][0]["sg_path_to_frames"] = None
                take["take_in"] = rvc.getIntProperty("{}.group.rangeOffset".format(source))[0] + 1
                duration = int(
                    {x[0]: x[1] for x in rvc.sourceAttributes(source, take["file_versions"][0]["sg_path_to_movie"])}["Duration"].split()[0])
                take["take_out"] = take["take_in"] + duration - 1
            take["file_versions"][0]["code"] = os.path.basename(media)
            take["file_versions"][0]["sg_first_frame"] = take["take_in"]
            take["file_versions"][0]["sg_last_frame"] = take["take_out"]
            media = rvc.getStringProperty("{}.media.movie".format(sources[this_index]))
            take["file_versions"][0]["sg_path_to_audio"] = len(media) > 1 and media[1].endswith(".wav") and media[1]
            shots = rvc.getStringProperty("{}.attributes.shots".format(source))[0]
            take["shots"] = shots and shots.split(", ")
            sequence_ins = rvc.getStringProperty("{}.attributes.sequenceIn".format(source))[0]
            take["sequence_in"] = sequence_ins and [int(x) for x in sequence_ins.split(", ")]
            takes[this_index] = take
        for frame, index, start, end in zip(frames, indexes, ins, outs):
            take = takes[index]
            sequence = take["sequence_in"] and take["sequence_in"].pop(0) or frame
            source = sources[index]
            if take["file_versions"][0]["sg_path_to_audio"]:
                offset = int(round(rvc.getFloatProperty("{}.group.audioOffset".format(source))[0] * 24 + (sequence - start + take["take_in"])))
                duration = int(
                    {x[0]: x[1] for x in rvc.sourceAttributes(source, take["file_versions"][0]["sg_path_to_audio"])}["Duration"].split()[0])
                audio = {"path": take["file_versions"][0]["sg_path_to_audio"], "offset": offset, "duration": duration}
            cut_item = {"id": 0,
                        "type": "CutItem",
                        "code": take["shots"] and take["shots"].pop(0) or take["name"],
                        "cut_item_in": start,
                        "cut_item_out": end,
                        "sg_track": 0,
                        "shot": None,
                        "cut_order": 0,
                        "edit_in": sequence,
                        "edit_out": sequence + (end - start),
                        "sg_take": None,
                        "version": take,
                        "sg_original_media": None,
                        "audio": audio}
            cut_items.append(cut_item)

        if cut_items:

            for cut_item in cut_items:
                new_widget = ShotWidget(self, self.dialog, None)
                new_item, new_widget = self.add_item(shot_widget=new_widget)
                new_widget.list_item = new_item
                new_widget.shot_changed.connect(self.set_edl)
                new_widget.shot_from_cut_item(cut_item)

    def cut_from_json(self, path=None, json_obj=None):
        json_data = None
        if path:
            with open(path, "r") as json_file:
                json_data = json.load(json_file)
        elif json_obj:
            json_data = json_obj
        takes = {}
        audio = json_data["audio"]
        sequence = json_data["sequence"]
        for take_data in json_data["takes"]:
            movie_path = None
            if "temp_path" in take_data:
                movie_path = take_data["temp_path"]
            if movie_path:
                file_name = os.path.basename(movie_path)
            else:
                take_path = os.path.join(os.path.dirname(path), take_data["name"])
                file_name = [x for x in os.listdir(take_path) if x.endswith(".mov")]
                file_name.sort(key=lambda x: os.path.getctime(os.path.join(take_path, x)), reverse=True)
                file_name = file_name[0]
                movie_path = os.path.join(take_path, file_name)
            take = next((x for x in ShotgunCaches.TAKE_CACHE if x["code"] == take_data["name"]), None)
            if not take:
                take = {"code": take_data["name"],
                        "type": "CustomEntity06",
                        "id": 0}
            if not os.path.exists(movie_path):
                raise Exception("Cannot find file.")
            take["name"] = take["code"]
            take["sg_sequence"] = sequence
            take["sg_take_in"] = take_data["take_in"]
            take["sg_take_out"] = take_data["take_out"]
            take["file_versions"] = [{"code": file_name,
                                      "id": 0,
                                      "sg_first_frame": take_data["take_in"],
                                      "sg_last_frame": take_data["take_out"],
                                      "sg_path_to_movie": movie_path,
                                      "sg_path_to_frames": None,
                                      "sg_path_to_audio": take_data.get("audio"),
                                      "sg_audio_offset": take_data.get("audio_offset"),
                                      "sg_sequence": sequence}]
            takes[take_data["name"]] = take
        cut_items = []
        for shot in json_data["shots"]:
            take = takes[shot["take"]]
            cut_item = {"id": 0,
                        "type": "CutItem",
                        "code": shot["name"],
                        "cut_item_in": shot["shot_in"],
                        "cut_item_out": shot["shot_out"],
                        "sg_track": shot["track"],
                        "shot": None,
                        "cut_order": shot["order"],
                        "edit_in": shot["sequence_in"],
                        "edit_out": shot["sequence_out"],
                        "sg_take": None,
                        "version": take,
                        "sg_original_media": None,
                        "audio": audio}
            cut_items.append(cut_item)
        return cut_items

    def focusInEvent(self, event):
        focus_item = self.itemAt(self.mapFromGlobal(QtGui.QCursor.pos()))
        if focus_item:
            self.setCurrentItem(focus_item)
        else:
            self.clearSelection()

    def add_item(self, item=None, shot_widget=None):
        if not item:
            item = QtGui.QListWidgetItem(self)
        item.setSizeHint(QtCore.QSize(73, 34))
        item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        if not shot_widget:
            shot_widget = LoadWidget(self, self.dialog, item)
        self.setItemWidget(item, shot_widget)
        model = self.model()
        if model:
            model.layoutChanged.connect(self.set_edl)
        self.repaint()
        return item, shot_widget

    def remove_item(self, items=None):
        if not items:
            items = self.selectedItems()
        for item in items:
            widget = self.itemWidget(item)
            if type(widget) is ShotWidget:
                used = False
                for ii in range(self.count()):
                    if ii == self.row(item):
                        continue
                    other_item = self.item(ii)
                    other_widget = self.itemWidget(other_item)
                    if type(widget) is ShotWidget and widget.source_group:
                        if widget.source_group == other_widget.source_group:
                            used = True
                if not used:
                    try:
                        rvc.deleteNode(widget.source_group)
                    except:
                        pass
            self.takeItem(self.row(item))
        self.set_edl()

    def clear_list(self):
        self.clear()
        for node in rvc.nodesOfType("RVSourceGroup"):
            rvc.deleteNode(node)

    def refresh(self):
        for node in rvc.nodesOfType("RVSourceGroup"):
            rvc.deleteNode(node)
        for ii in range(self.count()):
            item = self.item(ii)
            widget = self.itemWidget(item)
            widget.load_media()

    def replace_widget(self, list_item, new_widget):
        widget_size = new_widget.size()
        list_item.setSizeHint(widget_size)
        self.setItemWidget(list_item, new_widget)
        new_widget.shot_changed.connect(self.set_edl)
        
    def _on_selection_change(self):
        for ii in range(self.count()):
            item = self.item(ii)
            widget = self.itemWidget(item)
            if widget and widget.entity_type == "shot" and widget.shot:
                if item.isSelected():
                    widget.setStyleSheet(shot_widget.LOADED_SELECTED)
                else:
                    widget.setStyleSheet(shot_widget.LOADED_DESELECTED)

    def update_local(self, selected_items, user=None):
        if not selected_items:
            selected_items = self.selectedItems()
        for item in selected_items:
            widget = self.itemWidget(item)
            widget.update_local(user)

    def shot_from_edl(self):
        frame_list = rvc.getIntProperty("Playlist_sequence.edl.frame")
        previous_index = 0
        for ii, frame in enumerate(frame_list):
            if rvc.frame() < frame:
                return previous_index
            previous_index = ii

    def toggle_show_shot_names(self):
        value = not self.show_shot_names
        self.show_shot_names = value
        for ii in range(self.count()):
            item = self.item(ii)
            widget = self.itemWidget(item)
            rvc.setIntProperty("{}.overlay.show".format(widget.overlay), [value], True)

    def update_overlay(self, event, force=False):
        if not self.show_shot_names:
            return
        target_shot = self.shot_from_edl()
        current_shot = 0
        if target_shot is not None and (target_shot != self.current_shot or force):
            self.current_shot = target_shot
            for ii in range(self.count()):
                item = self.item(ii)
                widget = self.itemWidget(item)
                if type(widget) is not ShotWidget or not widget.ui.enabled_chk.isChecked():
                    continue
                if current_shot == target_shot:
                    widget.create_text(widget.overlay + ".text:shot", widget.ui.name_box.text(), 0, -0.43)
                    break
                current_shot += 1

    def set_edl(self):
        order = 1
        start = None
        frame = []
        source = []
        shot_in = []
        shot_out = []
        sources = rvc.nodeConnections("Playlist")
        if self.count():
            for ii in range(self.count()):
                item = self.item(ii)
                widget = self.itemWidget(item)
                if type(widget) is not ShotWidget or not widget.ui.enabled_chk.isChecked():
                    continue
                if not start:
                    start = (widget.sequence_start and
                             widget.sequence_start + (widget.shot.shot_in - widget.ui.shotin_box.value()) or
                             1001)
                if type(widget) is ShotWidget and widget.source_group and widget.source_group in sources[0]:
                    frame.append(start)
                    source.append(sources[0].index(widget.source_group))
                    shot_in.append(widget.shotin)
                    shot_out.append(widget.shotout)
                    start += (widget.shotout - widget.shotin + 1)
                    widget.ui.order_lbl.setText(str(order))
                    order += 1
            rvc.setIntProperty("Playlist_sequence.mode.useCutInfo", [0], True)
            rvc.setIntProperty("Playlist_sequence.mode.autoEDL", [0], True)
            rvc.setIntProperty("Playlist_sequence.output.autoSize", [1], True)
            rvc.setIntProperty("Playlist_sequence.edl.frame", frame + [start], True)
            rvc.setIntProperty("Playlist_sequence.edl.source", source + [0], True)
            rvc.setIntProperty("Playlist_sequence.edl.in", shot_in + [0], True)
            rvc.setIntProperty("Playlist_sequence.edl.out", shot_out + [0], True)

        else:
            rvc.setNodeInputs("Playlist", [])
            rvc.setIntProperty("Playlist_sequence.mode.useCutInfo", [0], True)
            rvc.setIntProperty("Playlist_sequence.mode.autoEDL", [0], True)
            rvc.setIntProperty("Playlist_sequence.output.autoSize", [0], True)
            rvc.setIntProperty("Playlist_sequence.edl.frame", [], True)
            rvc.setIntProperty("Playlist_sequence.edl.source", [], True)
            rvc.setIntProperty("Playlist_sequence.edl.in", [], True)
            rvc.setIntProperty("Playlist_sequence.edl.out", [], True)
