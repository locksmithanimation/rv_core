from locksmith_dev import build


if __name__ == "__main__":

    bld = build.RezBuild(
        python_modules=["python"],
        resources=["support_path", "bin"]
    )
    bld.print_summary()

    print("Building package...")

    # Build and install Python modules and resource files.
    bld.build_install_python()
    bld.build_install_resources()
