#!/usr/bin/python
import os
import sys
import traceback
import subprocess

def main(args):
    path = sorted([x for x in os.listdir("C:/Program Files/Shotgun") if x.startswith("RV-")])[-1]
    subprocess.call('"{}" -uninstall "Locksmith Playlist"'.format(os.path.join("C:/Program Files/Shotgun", path, "bin", "rvpkg")))
    subprocess.call('"{}" -install "Locksmith Playlist"'.format(os.path.join("C:/Program Files/Shotgun", path, "bin", "rvpkg")))
    subprocess.call("start rv {}".format(args and args[0] or ""), shell=True)

main(sys.argv[1:])
