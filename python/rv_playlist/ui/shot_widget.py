# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'W:\DEPARTMENTS\DEV\willw\packages\rv\python\rv_playlist\resources\shot_widget.ui'
#
# Created: Tue Jan 15 13:12:08 2019
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

class Ui_ShotWidget(object):
    def setupUi(self, ShotWidget):
        ShotWidget.setObjectName("ShotWidget")
        ShotWidget.resize(325, 34)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ShotWidget.sizePolicy().hasHeightForWidth())
        ShotWidget.setSizePolicy(sizePolicy)
        ShotWidget.setMinimumSize(QtCore.QSize(325, 0))
        ShotWidget.setMaximumSize(QtCore.QSize(16000, 70))
        ShotWidget.setMouseTracking(True)
        ShotWidget.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        ShotWidget.setAcceptDrops(True)
        ShotWidget.setStyleSheet("QFrame{background-color:rgb(115, 166, 202)}\n"
"QLineEdit{background-color:rgb(255, 255, 255, 0);\n"
"                color:black;}\n"
"QComboBox{background-color:rgb(255, 255, 255, 0);\n"
"                     color:black;}\n"
"QComboBox:focus{color:black;}\n"
"QComboBox QAbstractItemView{color:black;}\n"
"QLabel{color:black;}\n"
"QSpinBox{background-color:rgb(255, 255, 255, 0);\n"
"                 color:black;}")
        ShotWidget.setFrameShape(QtGui.QFrame.Panel)
        ShotWidget.setFrameShadow(QtGui.QFrame.Raised)
        ShotWidget.setLineWidth(1)
        self.verticalLayout_2 = QtGui.QVBoxLayout(ShotWidget)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setSpacing(2)
        self.horizontalLayout_5.setContentsMargins(5, 5, 5, 5)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.enabled_chk = QtGui.QCheckBox(ShotWidget)
        self.enabled_chk.setStyleSheet("QCheckBox{background-color:rgb(255, 255, 255, 0);\n"
"                    color:black;}")
        self.enabled_chk.setText("")
        self.enabled_chk.setChecked(True)
        self.enabled_chk.setObjectName("enabled_chk")
        self.horizontalLayout_5.addWidget(self.enabled_chk)
        self.order_lbl = QtGui.QLabel(ShotWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.order_lbl.sizePolicy().hasHeightForWidth())
        self.order_lbl.setSizePolicy(sizePolicy)
        self.order_lbl.setMinimumSize(QtCore.QSize(20, 0))
        self.order_lbl.setMaximumSize(QtCore.QSize(20, 16777215))
        self.order_lbl.setText("")
        self.order_lbl.setObjectName("order_lbl")
        self.horizontalLayout_5.addWidget(self.order_lbl)
        self.name_box = QtGui.QLineEdit(ShotWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.name_box.sizePolicy().hasHeightForWidth())
        self.name_box.setSizePolicy(sizePolicy)
        self.name_box.setMinimumSize(QtCore.QSize(90, 18))
        self.name_box.setMaximumSize(QtCore.QSize(110, 18))
        font = QtGui.QFont()
        font.setFamily("MS UI Gothic")
        font.setPointSize(7)
        self.name_box.setFont(font)
        self.name_box.setStyleSheet("")
        self.name_box.setText("")
        self.name_box.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.name_box.setObjectName("name_box")
        self.horizontalLayout_5.addWidget(self.name_box)
        self.version_cmb = QtGui.QComboBox(ShotWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.version_cmb.sizePolicy().hasHeightForWidth())
        self.version_cmb.setSizePolicy(sizePolicy)
        self.version_cmb.setMinimumSize(QtCore.QSize(100, 0))
        self.version_cmb.setObjectName("version_cmb")
        self.horizontalLayout_5.addWidget(self.version_cmb)
        spacerItem = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem)
        self.shotin_box = QtGui.QSpinBox(ShotWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.shotin_box.sizePolicy().hasHeightForWidth())
        self.shotin_box.setSizePolicy(sizePolicy)
        self.shotin_box.setMinimumSize(QtCore.QSize(35, 18))
        self.shotin_box.setMaximumSize(QtCore.QSize(35, 18))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.shotin_box.setFont(font)
        self.shotin_box.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.shotin_box.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.shotin_box.setMaximum(999999)
        self.shotin_box.setObjectName("shotin_box")
        self.horizontalLayout_5.addWidget(self.shotin_box)
        self.shotout_box = QtGui.QSpinBox(ShotWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.shotout_box.sizePolicy().hasHeightForWidth())
        self.shotout_box.setSizePolicy(sizePolicy)
        self.shotout_box.setMinimumSize(QtCore.QSize(35, 18))
        self.shotout_box.setMaximumSize(QtCore.QSize(35, 18))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.shotout_box.setFont(font)
        self.shotout_box.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.shotout_box.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.shotout_box.setMaximum(999999)
        self.shotout_box.setProperty("value", 0)
        self.shotout_box.setObjectName("shotout_box")
        self.horizontalLayout_5.addWidget(self.shotout_box)
        self.horizontalLayout_5.setStretch(2, 2)
        self.horizontalLayout_5.setStretch(3, 3)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.retranslateUi(ShotWidget)
        QtCore.QMetaObject.connectSlotsByName(ShotWidget)

    def retranslateUi(self, ShotWidget):
        ShotWidget.setWindowTitle(QtGui.QApplication.translate("ShotWidget", "Frame", None, QtGui.QApplication.UnicodeUTF8))

