import operator
import sgtk
from tank_vendor import shotgun_api3

class ShotgunCaches(object):
    SHOTGUN = None
    CUT_CACHE = None
    TAKE_CACHE = None
    SEQUENCE_CACHE = None

    @staticmethod
    def init_caches():
        base_url = "https://locksmith.shotgunstudio.com"
        script_name = "RvPlaylist"
        api_key = "bicdwqwrvokcBb7otyt#xoecc"
        ShotgunCaches.SHOTGUN = shotgun_api3.Shotgun(base_url, script_name, api_key)

        ShotgunCaches.CUT_CACHE = ShotgunCaches.SHOTGUN.find("Cut",
                                          [["project", "is", {"type": "Project", "id": 158}]],
                                          ["revision_number", "code", "description",
                                           "entity", "created_at", "sg_type", "cut_items"])
        ShotgunCaches.CUT_CACHE = sorted(ShotgunCaches.CUT_CACHE, key=operator.itemgetter("code"))

        ShotgunCaches.TAKE_CACHE = ShotgunCaches.SHOTGUN.find("CustomEntity06",
                                           [["project", "is", {"type": "Project", "id": 158}]],
                                           ["sg_sequence", "sg_action", "code", "sg_take_in",
                                            "sg_take_out", "sg_versions"])
        ShotgunCaches.TAKE_CACHE = sorted(ShotgunCaches.TAKE_CACHE, key=operator.itemgetter("code"))

        ShotgunCaches.SEQUENCE_CACHE = ShotgunCaches.SHOTGUN.find("Sequence",
                                               [["project", "is", {"type": "Project", "id": 158}]],
                                               ["code", "image"])
        ShotgunCaches.SEQUENCE_CACHE = sorted(ShotgunCaches.SEQUENCE_CACHE , key=operator.itemgetter("code"))
